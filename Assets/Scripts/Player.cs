﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private int life;
    [SerializeField] private float speed;
    [SerializeField] private float diedDelay;
    [SerializeField] private GameObject ballGO;
    [SerializeField] private Transform startPlayer;
    [SerializeField] private float maxDistanceX;

    private int score;
    private bool isDied;
    private Ball ball;

    public UnityEvent ReceiveLifeEvent { get; set; } = new UnityEvent();
    public UnityEvent ReceiveScoreEvent { get; set; } = new UnityEvent();

    public int Life
    {
        get => life;
        set
        {
            if (value <= 0)
            {
                life = 0;
                GameManager.Instance.FinishGame(false);
            }
            else
                life = value;

            ReceiveLifeEvent?.Invoke();
        }
    }

    public int Score
    {
        get => score;
        set
        {
            score = value;
            ReceiveScoreEvent?.Invoke();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        isDied = false;
        ball = ballGO.GetComponent<Ball>();
        SetBallPosition();

        Ball.OnBallKilledAsStatic += Died;
        GameManager.Instance.Scene = GameManager.SceneGame.GamePlay;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDied)
        {
            Move();
            ShootBall();
        }
    }

    void OnDestroy()
    {
        Ball.OnBallKilledAsStatic -= Died;
    }

    void Move()
    {
        if (Input.anyKey)
        {
            if (Input.GetAxis("Horizontal") != 0)
            {
                Vector3 auxPos = transform.position;

                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                {
                    if (CheckMove(-transform.right))
                    {
                        auxPos.x -= speed * Time.deltaTime;
                    }
                }
                else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                {
                    if (CheckMove(transform.right))
                    {
                        auxPos.x += speed * Time.deltaTime;
                    }
                }

                transform.position = auxPos;

                if (!ball.isMoving)
                {
                    SetBallPosition();
                }
            }
        }
    }

    void ShootBall()
    {
        if (!ball.isMoving)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                float distance = startPlayer.position.x - transform.position.x;

                float percentX = 100 - Mathf.Abs(distance) * 100 / maxDistanceX;
                float ballAngleMax = 90;
                float ballAngleMin = 20;

                float angle = ballAngleMin + ((ballAngleMax - ballAngleMin) * percentX / 100);

                if (distance > 0)
                    angle *= -1f;

                ball.StartForce(angle);
            }
        }
    }

    void SetBallPosition()
    {
        ball.transform.position = transform.position + Vector3.up * ball.offsetPlayer;
    }

    bool CheckMove(Vector3 direction)
    {
        float distance = transform.localScale.x / 2;
        return !Physics.Raycast(transform.position, direction, distance);
    }

    public void Died()
    {
        isDied = true;
        StartCoroutine(PlayerDeath());
    }

    IEnumerator PlayerDeath()
    {
        yield return new WaitForSeconds(diedDelay);

        Life--;
        isDied = false;
        transform.position = startPlayer.position;
        
        SetBallPosition();
    }
}