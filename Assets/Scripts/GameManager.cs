﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int MainMenuScene { get; } = 0;

    public int GamePlayScene { get; } = 1;

    public int GameOverScene { get; } = 2;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public string HighScoreKey { get; } = "highscore";

    public enum SceneGame
    {
        MainMenu,
        GamePlay,
        GameOver
    }

    public bool Win { get; set; } = false;
    public int Score { get; set; } = 0;
    public int HighScore { get; set; } = 0;

    public SceneGame Scene { get; set; }

    public bool GameOver { get; set; }

    public void FinishGame(bool winGame)
    {
        Score = FindObjectOfType<Player>().Score;
        Win = winGame;
        GameOver = true;
        ChangeScene(SceneGame.GameOver);
    }

    public void ChangeScene(SceneGame s)
    {
        int sceneValue = 0;
        switch (s)
        {
            case SceneGame.MainMenu:
                sceneValue = MainMenuScene;
                break;
            case SceneGame.GamePlay:
                sceneValue = GamePlayScene;
                break;
            case SceneGame.GameOver:
                sceneValue = GameOverScene;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        Scene = s;
        SceneManager.LoadScene(sceneValue);
    }

    public void RestartGame()
    {
        GameOver = false;
        Win = false;
        Score = 0;
        HighScore = PlayerPrefs.GetInt(HighScoreKey);
    }
}
