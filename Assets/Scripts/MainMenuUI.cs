﻿using System;
using UnityEngine;

public class MainMenuUI : MonoBehaviour
{
    void Start()
    {
        GameManager.Instance.Scene = GameManager.SceneGame.MainMenu;
    }

    public void PlayGame()
    {
        GameManager.Instance.ChangeScene(GameManager.SceneGame.GamePlay);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
