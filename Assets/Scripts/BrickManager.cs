﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrickManager : MonoBehaviour
{
    [SerializeField] private GameObject brickPrefab;
    [SerializeField] private List<Color> colors = new List<Color>();
    [SerializeField] private int col;
    [SerializeField] private int row;
    [SerializeField] private float width;

    private List<Brick> brickList = new List<Brick>();

    // Start is called before the first frame update
    void Start()
    {
        float w = width / col;
        float offsetX = w / 2;
        Vector3 auxScale = new Vector3();

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                GameObject brick = Instantiate(brickPrefab);

                Brick b = brick.GetComponent<Brick>();
                brick.name = "Brick " + (brickList.Count + 1);

                int index = Random.Range(0, colors.Count);
                brick.GetComponent<Renderer>().material.color = colors[index];

                auxScale = brick.transform.localScale;
                auxScale.x = w;
                brick.transform.localScale = auxScale;

                Vector3 auxPos = transform.position;
                auxPos.x = transform.position.x + offsetX + (w * j);
                auxPos.y = transform.position.y + i;
                brick.transform.position = auxPos;

                brick.transform.parent = transform;

                brickList.Add(b);
            }
        }

        Brick.OnBrickKilledAsStatic += RemoveBrick;
    }

    void OnDestroy()
    {
        Brick.OnBrickKilledAsStatic -= RemoveBrick;
    }

    void RemoveBrick(Brick b)
    {
        brickList.Remove(b);

        if (brickList.Count == 0)
            GameManager.Instance.FinishGame(true);
    }
}
