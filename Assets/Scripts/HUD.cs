﻿using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    [SerializeField] TMP_Text score;
    [SerializeField] TMP_Text life;

    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();

        player.ReceiveLifeEvent.AddListener(SetLife);
        player.ReceiveScoreEvent.AddListener(SetScore);

        player.ReceiveLifeEvent?.Invoke();
    }

    void OnDestroy()
    {
        player.ReceiveLifeEvent.RemoveListener(SetLife);
        player.ReceiveScoreEvent.RemoveListener(SetScore);
    }

    void SetLife()
    {
        life.text = player.Life.ToString();
    }

    void SetScore()
    {
        score.text = "Score: " + player.Score.ToString();
    }
}
