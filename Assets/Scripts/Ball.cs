﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] [Range(1, 100)] private float increaseSpeed;
    [SerializeField] public float offsetPlayer;
    [SerializeField] private float minY;
    [SerializeField] private LayerMask maskBrick;
    
    [HideInInspector] public bool isMoving;
    private Rigidbody rb;

    public delegate void BallKilledAction();
    public static BallKilledAction OnBallKilledAsStatic;

    // Start is called before the first frame update
    void Start()
    {
        isMoving = false;
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (isMoving)
        {
            if (CheckPositionY())
            {
                Restart();
            }
        }
    }

    bool CheckPositionY()
    {
        return transform.position.y <= minY;
    }

    public void StartForce(float angle)
    {
        Vector2 auxVelocity;

        auxVelocity.x = speed * Mathf.Cos(angle * Mathf.PI / 180);
        auxVelocity.y = Mathf.Sqrt(Mathf.Pow(speed, 2) - Mathf.Pow(auxVelocity.x, 2));

        if (angle < 0)
            auxVelocity.x *= -1f;

        rb.AddForce(new Vector3(auxVelocity.x, auxVelocity.y, 0), ForceMode.Impulse);
        isMoving = true;
    }

    void Restart()
    {
        if (OnBallKilledAsStatic != null)
            OnBallKilledAsStatic();
        
        isMoving = false;
        rb.velocity = Vector3.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        rb.AddForce(rb.velocity.normalized * (increaseSpeed / 10), ForceMode.Impulse);

        if (CheckLayerInMask(maskBrick, collision.gameObject.layer))
        {
            collision.gameObject.GetComponent<Brick>().DestroyBrick();
        }
    }

    public bool CheckLayerInMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}