﻿using UnityEngine;
using TMPro;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] TMP_Text result;
    [SerializeField] TMP_Text score;
    [SerializeField] TMP_Text highScore;

    // Start is called before the first frame update
    void Start()
    {
        string highScoreKey = GameManager.Instance.HighScoreKey;

        GameManager.Instance.HighScore = PlayerPrefs.GetInt(highScoreKey);
        if (GameManager.Instance.Score > GameManager.Instance.HighScore)
        {
            PlayerPrefs.SetInt(highScoreKey, GameManager.Instance.Score);
            highScore.text = "NEW HIGHSCORE: " + GameManager.Instance.Score;
            score.text = "";
        }
        else
        {
            highScore.text = "HIGHSCORE: " + GameManager.Instance.HighScore;
            score.text = "You Score: " + GameManager.Instance.Score;
        }

        result.text = GameManager.Instance.Win ? "YOU WIN!" : "YOU LOSE!";

        GameManager.Instance.Scene = GameManager.SceneGame.GameOver;
    }

    public void BackToMenu()
    {
        GameManager.Instance.RestartGame();
        GameManager.Instance.ChangeScene(GameManager.SceneGame.MainMenu);
    }
}
