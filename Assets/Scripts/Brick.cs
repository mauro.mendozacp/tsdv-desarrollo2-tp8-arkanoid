﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [SerializeField] public int points;

    public delegate void BrickKilledAction(Brick b);
    public static BrickKilledAction OnBrickKilledAsStatic;

    public void DestroyBrick()
    {
        if (OnBrickKilledAsStatic != null)
            OnBrickKilledAsStatic(this);

        FindObjectOfType<Player>().Score += points;

        Destroy(gameObject);
    }
}
